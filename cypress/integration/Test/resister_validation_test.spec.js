describe('MyTestSuite',function(){

  //load external file of testData before start test case
  before(function(){

    cy.fixture('example.json').then(function(testdata){

      this.data=testdata

    })

  })
  
  
  it('ValidationRegister', function(){

    
    //navigate to the website
    cy.visit('https://www.ta3limy.com/')


    //allocate Register button "تسجيل حساب".
    cy.get('div.css-9nqq2c.e1dkxuud0 div.css-1r3273x.e6hmffv0:nth-child(11) div.css-5s4uu1.edzx4801 section.css-8lg940.e1a5eqzl1 button.e1a5eqzl0.css-1iul13l.erkcdwb4 > a:nth-child(1)').click()


    //check the opened page is register page by checking the text of "مرحبا بك في منصة تعليمي".
    cy.contains('مرحباً بك فى منصة تعليمى')


     //Select the "طالب" button.
     cy.get('div.css-9nqq2c.e1dkxuud0 section.css-9zd367.e16kpnuv0:nth-child(10) div.css-1pc5j26.e16kpnuv1 form.css-1xdhyk6.e11pp5v82 fieldset.css-1m9i1li.e11pp5v80 fieldset.css-r4qrkr.e1dq7k072:nth-child(1) div.css-1wypkgy.e1dq7k071 div.css-hkbe8x.e1h2vq1b1:nth-child(2) > label.css-ubxa2z.e185xscr0').click()

    //Allocate firstName field and write the value inside it.
    cy.get('#firstName').should('be.visible').should('be.enabled').type(this.data.firstName) //getting value of firstName from external data file

    //Allocate lastName field and write the value inside it.
    cy.get('#lastName').should('be.visible').should('be.enabled').type(this.data.lastName) //getting value of lastName from external data file


    //Allocate parent's phone number and write the value inside it.
    cy.get('#mobileNumber').should('be.visible').should('be.enabled').type(this.data.parentNumber,{force:true}) //getting value of parentNumber from external data file

    //Select gender of the student.
    //cy.get('#male', {force:true}).click()

    cy.wait(800)

    cy.get('div.css-9nqq2c.e1dkxuud0 section.css-9zd367.e16kpnuv0:nth-child(10) div.css-1pc5j26.e16kpnuv1 form.css-1xdhyk6.e11pp5v82 fieldset.css-1m9i1li.e11pp5v80 fieldset.css-r4qrkr.esvl4ai2:nth-child(5) div.css-14hq4q9.esvl4ai1 div.css-hkbe8x.e1h2vq1b1:nth-child(1) > label.css-ubxa2z.e185xscr0',{force:true}).should('be.visible').click()


    //Allocate dropdown list and click on it to choose the appropiate value.
    cy.get('select').select('9').should('have.value', '9')


    //Allocate password field and write value inside the field.
    cy.get('#password').type(this.data.password) //getting value of password from external data file

    //Allocate confirm password field and write value inside the field.
    cy.get('#passwordConfirmation').type(this.data.confirmationPassword) //getting value of confirmationPassword from external data file


    //Check radio button of the agree terms
    cy.get('#termsAndConditionsCheck').check({ force: true }).should('be.checked')


    //Check checkbox button of "I am not a robot"
    cy.wait(8000)
    cy.window().then(win => {
      win.document
        .querySelector("iframe[src*='recaptcha']") 
        .contentDocument.getElementById("recaptcha-token")
        .click();
    });

    /*Cypress.Commands.add('solveGoogleReCAPTCHA', () => {
      // Wait until the iframe (Google reCAPTCHA) is totally loaded
      cy.wait(1000);
      cy.get('#g-recaptcha *> iframe')
        .then($iframe => {
          const $body = $iframe.contents().find('body');
          cy.wrap($body)
            .find('.recaptcha-checkbox-border')
            .should('be.visible')
            .click();
        });
    });*/

    
    //Click on Resiter button
    cy.wait(10000)
    cy.get('div.css-9nqq2c.e1dkxuud0 section.css-9zd367.e16kpnuv0:nth-child(10) div.css-1pc5j26.e16kpnuv1 form.css-1xdhyk6.e11pp5v82 > button.e11pp5v81.css-kjrfbb.erkcdwb4',{force: true}).click()

    //cy.get('button[type=submit]').click()
    
    //cy.get('form').submit()



  })

})