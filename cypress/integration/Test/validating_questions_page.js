
/// <refernce types="cypress" />
describe('My First Test', () => {
  it('finds the content "type"', () => {
    cy.visit('https://www.ta3limy.com/login')
    cy.title().should('eq', 'منـصــة تعليمى')

    //Locating and clicking on ""
    cy.get('div.css-9nqq2c.e1dkxuud0 header.css-1viif4q.e7p2mdl0:nth-child(9) div.css-1bwnzz7.ehmrgki0 div.css-12xhfvc.e7p2mdl1 div.css-79elbk.e14inxmz0 div.css-11r87u1.e14inxmz1 div:nth-child(1) svg:nth-child(1) > path:nth-child(3)').click()
    

    cy.get('div.css-9nqq2c.e1dkxuud0 header.css-1viif4q.e7p2mdl0:nth-child(9) div.css-1bwnzz7.ehmrgki0 div.css-12xhfvc.e7p2mdl1 div.css-79elbk.e14inxmz0 ul.css-1bearfn.e14inxmz2 li.link:nth-child(1) > a:nth-child(2)').click()

    cy.get("div.css-9nqq2c.e1dkxuud0 section.css-1ntuz6x.e19vwfi41:nth-child(10) div.css-1cls8ev.e19vwfi42 section.css-lfvfxp.e19vwfi43 div.Collapsible:nth-child(1) div.Collapsible__contentOuter div.Collapsible__contentInner > p:nth-child(1)").should('not.be.visible')
   
    // clicking on "ما هي المؤسسة؟" question
    cy.get(' div.css-9nqq2c.e1dkxuud0 section.css-1ntuz6x.e19vwfi41:nth-child(10) div.css-1cls8ev.e19vwfi42 section.css-lfvfxp.e19vwfi43 div.Collapsible:nth-child(1) span.Collapsible__trigger.is-closed > p.css-olkrgj.e1n5lxyo0').click() 


    /*cy.get("div.css-9nqq2c.e1dkxuud0 section.css-1ntuz6x.e19vwfi41:nth-child(10) div.css-1cls8ev.e19vwfi42 section.css-lfvfxp.e19vwfi43 div.Collapsible:nth-child(1) div.Collapsible__contentOuter div.Collapsible__contentInner > p:nth-child(1)").should('be.visible')*/


    //validating on "ما هي المؤسسة؟" answer
    cy.get("div.css-9nqq2c.e1dkxuud0 section.css-1ntuz6x.e19vwfi41:nth-child(10) div.css-1cls8ev.e19vwfi42 section.css-lfvfxp.e19vwfi43 div.Collapsible:nth-child(1) div.Collapsible__contentOuter div.Collapsible__contentInner > p:nth-child(1)").contains("مؤسسة ڤودافون مصر لتنمية المجتمع هي أول مؤسسة لا تهدف للربح في مجال الاتصالات في مصر وهي تابعة لوزارة التضامن الاجتماعي وتختلف عن قسم المسؤولية المجتمعية الخاص بالشركة. من وقت تأسيسها في سنة 2003 والمؤسسة بتحاول جاهدة إنها تنمي المجتمع من خلال إشراك منظمات المجتمع المدني والهيئات الغير حكومية في عملية تطوير المجتمع المصري في مجالات التعليم وتمكين ذوي الاحتياجات الخاصة.")  

    // clicking on "ما هو تعليمي " question
    cy.get('div.css-9nqq2c.e1dkxuud0 section.css-1ntuz6x.e19vwfi41:nth-child(10) div.css-1cls8ev.e19vwfi42 section.css-lfvfxp.e19vwfi43 div.Collapsible:nth-child(2) span.Collapsible__trigger.is-closed > p.css-olkrgj.e1n5lxyo0').click()


    //validating on "ما هو تعليمي؟" question
    cy.get('div.css-9nqq2c.e1dkxuud0 section.css-1ntuz6x.e19vwfi41:nth-child(10) div.css-1cls8ev.e19vwfi42 section.css-lfvfxp.e19vwfi43 div.Collapsible:nth-child(2) div.Collapsible__contentOuter div.Collapsible__contentInner > p:nth-child(1)').contains("تعليمي هي منصة إلكترونية تعليمية مجانية للطلاب وأولياء الأمور والمعلمين")


    //clicking on "من المستفيد من تعليمي؟" question
    cy.get('div.css-9nqq2c.e1dkxuud0 section.css-1ntuz6x.e19vwfi41:nth-child(10) div.css-1cls8ev.e19vwfi42 section.css-lfvfxp.e19vwfi43 div.Collapsible:nth-child(3) span.Collapsible__trigger.is-closed > p.css-olkrgj.e1n5lxyo0').click()

    //validating on "من المستفيد من تعليمي؟" question
    cy.get('div.css-9nqq2c.e1dkxuud0 section.css-1ntuz6x.e19vwfi41:nth-child(10) div.css-1cls8ev.e19vwfi42 section.css-lfvfxp.e19vwfi43 div.Collapsible:nth-child(3) div.Collapsible__contentOuter div.Collapsible__contentInner > p:nth-child(1)').contains("أولياء الأمور والطلاب في المرحلة الأولي وستستهدف المرحلة القادمة تقديم محتوى للمعلمين")

    
    //clicking on "ما هو المحتوى الموجود في تعليمي" question
    cy.get('div.css-9nqq2c.e1dkxuud0 section.css-1ntuz6x.e19vwfi41:nth-child(10) div.css-1cls8ev.e19vwfi42 section.css-lfvfxp.e19vwfi43 div.Collapsible:nth-child(4) span.Collapsible__trigger.is-closed > p.css-olkrgj.e1n5lxyo0').click()


    //clicking on "هل تعليمي مجاني؟" question
    cy.get('div.css-9nqq2c.e1dkxuud0 section.css-1ntuz6x.e19vwfi41:nth-child(10) div.css-1cls8ev.e19vwfi42 section.css-lfvfxp.e19vwfi43 div.Collapsible:nth-child(5) span.Collapsible__trigger.is-closed > p.css-olkrgj.e1n5lxyo0').click()


    //validating on "هل تعليمي مجاني؟" question
    cy.get('div.css-9nqq2c.e1dkxuud0 section.css-1ntuz6x.e19vwfi41:nth-child(10) div.css-1cls8ev.e19vwfi42 section.css-lfvfxp.e19vwfi43 div.Collapsible:nth-child(5) div.Collapsible__contentOuter div.Collapsible__contentInner > p:nth-child(1)').contains("تعليمي مجاني لجميع الأفراد بشكل عام ويتميز عملاء ڤودافون بعدم السحب من باقة الانترنت بشكل خاص")
    

    //clicking on "كيف أقوم بإستخدام تعليمي؟" question
    cy.get('div.css-9nqq2c.e1dkxuud0 section.css-1ntuz6x.e19vwfi41:nth-child(10) div.css-1cls8ev.e19vwfi42 section.css-lfvfxp.e19vwfi43 div.Collapsible:nth-child(6) span.Collapsible__trigger.is-closed > p.css-olkrgj.e1n5lxyo0').click()
  })
})